var to = require('../config');

suite('Configuration Test', function () {

  test('LDAP Server Setup', function () {
    to.LDAP_PORT.should.be.ok.and.a.Number.and.below(9999).and.above(0);
    to.LDAP_ADDRESS.should.be.ok.and.a.String;
  });

  test('Log Setup', function () {
    to.LOG_STREAMS.should.be.ok.and.an.Array;
    to.LOG_STREAMS.length.should.be.above(0);
  });

  test('User Setup', function () {
    to.USER.should.be.ok;
    to.USER.should.be.an.Object.and.should.not.have.a.lengthOf(0);
    for (var name in to.USER) {
      var user = to.USER[name];
      user.should.be.an.Object;
      user.pass.should.be.a.String;
      user.secret.should.be.a.String;
      user.groups.should.be.an.Array;
    }
  });

});

