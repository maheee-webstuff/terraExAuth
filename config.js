
exports.SRV_LDAP = {
  address: '127.0.0.1',
  port: 389,
  baseDn: 'dc=terraex,dc=de',
};

exports.SRV_HTTP = {
  address: '127.0.0.1',
  port: 8080,
};

exports.SRV_ADMIN = {
  address: '127.0.0.1',
  port: 8081,
};

exports.LOG_STREAMS = [
 {
   level: 'debug',
   stream: process.stdout,
 },
 {
   level: 'debug',
   path: 'log.log',
 },
];

exports.DATABASE = ['TINGO', 'MONGO'][0];
exports.DB_TINGO = {
  path: './db/',
  options: {},
};
exports.DB_MONGO = {
  server: 'localhost',
  port: 27017,
  name: 'terraExAuth'
};

///////////////////////////
exports.USER = {
  'admin': {
    pass: 'admin',
    secret: 'AAAAAAAA',
    groups: ['ADMIN', 'WP_USER'],
  },
  'mahe': {
    pass: 'soylentgreen',
    secret: 'ONXXS3DF',
    groups: ['WP_USER'],
  },
};
