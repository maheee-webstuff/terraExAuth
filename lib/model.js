var Hotp = function (key, keyEncoding, counter, length) {
  this.type = 'HOTP';
  this.key = key;
  this.keyEncoding = keyEncoding;
  this.counter = counter;
  this.length = length;
}

var Totp = function (key, keyEncoding, step, length) {
  this.type = 'TOTP';
  this.key = key;
  this.keyEncoding = keyEncoding;
  this.step = step;
  this.length = length;
}

var Password = function (hash, salt, algorithm) {
  this.type = 'PASS';
  this.hash = hash;
  this.salt = salt;
  this.algorithm = algorithm;
}

var User = function (name, groups) {
  this.name = name;
  this.groups = groups;
  this.separator = '_';
  this.measures = [];
}


exports.Hotp     = Hotp;
exports.Totp     = Totp;
exports.Password = Password;
exports.User     = User;

