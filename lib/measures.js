var c = require('./common.js');
var log = c.getLogger('http');

var model = require('./model.js');

var speakeasy = require('speakeasy');
var bcrypt = require('bcrypt-nodejs');


function createPasswordMeasure(pass) {
  var salt = bcrypt.genSaltSync();
  var hash = bcrypt.hashSync(pass, salt);
  return new model.Password(hash, salt, 'bcrypt');
}

function createTotpMeasure(key, keyEncoding, step, length) {
  return new model.Totp(key, keyEncoding, step, length);
}

function createHotpMeasure(key, keyEncoding, count, length) {
  return new model.Hotp(key, keyEncoding, count, length);
}

exports.createPasswordMeasure = createPasswordMeasure;
exports.createTotpMeasure = createTotpMeasure;
exports.createHotpMeasure = createHotpMeasure;


function verifyPasswordMeasure(measure, cred) {
  return bcrypt.compareSync(cred, measure.hash);
}

function verifyTotpMeasure(measure, cred) {
  var totp = speakeasy.totp({
    key: measure.key,
    encoding: measure.keyEncoding,
    step: measure.step,
    length: measure.length
  });
  return totp == cred;
}

function verifyHotpMeasure(measure, cred) {
  var hotp = speakeasy.hotp({
    key: measure.key,
    encoding: measure.keyEncoding,
    counter: measure.counter,
    length: measure.length
  });
  return hotp == cred;
}

function verifyMeasure(measure, cred) {
  log.debug("Trying measure type: " + measure.type);
  var result;
  try {
    switch (measure.type) {
      case 'PASS':
        result = verifyPasswordMeasure(measure, cred);
        break;
      case 'TOTP':
        result = verifyTotpMeasure(measure, cred);
        break;
      case 'HOTP':
        result = verifyHotpMeasure(measure, cred);
        break;
      default:
        result = false;
    }
  } catch (e) {
    log.error("Measure type " + measure.type + " failed: " + e);
    return false;
  }
  log.debug("Measure type " + measure.type + " resulted: " + result);
  return result;
}

function verifyMeasures(measures, creds) {
  if (measures.length !== creds.length || measures.length <= 0) {
    return false;
  }
  var i = 0;
  for (i = 0; i < measures.length; ++i) {
    if (!verifyMeasure(measures[i], creds[i])) {
      return false;
    }
  }
  return true;
}

exports.verifyPasswordMeasure = verifyPasswordMeasure;
exports.verifyTotpMeasure = verifyTotpMeasure;
exports.verifyHotpMeasure = verifyHotpMeasure;
exports.verifyMeasure = verifyMeasure;
exports.verifyMeasures = verifyMeasures;
