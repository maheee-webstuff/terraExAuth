var c = require('./common.js');
var log = c.getLogger('http');

var express = require('express');


exports.createServer = function () {
  return express();
};

exports.startServer = function (server, address, port) {
  server.listen(port, address, function () {
    log.info("HTTP server listening at: " + address + ":" + port);
  });
};

exports.setupEndpoints = function (server, getUser, checkCredentials) {
  server.get('/', function (req, res) {
    var user = req.query.user;
    var credentials = req.query.credentials;

    log.debug("Got request for user: " + user);

    if (!user || ! credentials) {
      res.status(400).end();
    } else {
      getUser(user, function (err, user) {
        if (err || ! user) {
          res.status(401).end();
        } else {
          checkCredentials(user, credentials, function (err, result) {
            if (err || !result) {
              res.status(401).end();
            } else {
              res.status(200).end();
            }
          });
        }
      });
    }
  });
};
