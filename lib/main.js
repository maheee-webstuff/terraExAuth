var c = require('./common.js');
var log = c.getLogger('main');

var ldap = require('./ldap.js');
var http = require('./http.js');
var model = require('./model.js');
var measures = require('./measures.js');

var db = c.getDatabase();
var userDb = db.collection('user');


/*********************************/
/*********************************/
var user = new model.User('mahe', ['G1', 'G2']);
user.measures.push(measures.createPasswordMeasure('password'));
user.measures.push(measures.createTotpMeasure('ONXXS3DF', 'base32', 30, 6));

user._id = user.name;
userDb.insert(user, function (err) {
  console.log(err);
  console.log("INSERTED");
});
/*********************************/
/*********************************/


function getUser(id, callback) {
  log.info("Search user in database: " + id);
  userDb.findOne({_id: id}, function (err, item) {
    if (err) {
      log.info("Didn't find user in database: " + id);
    } else {
      log.info("Found user in database: " + id);
    }
    callback(err, item);
  });
}

function checkCredentials(user, credentials, callback) {
  if (measures.verifyMeasures(user.measures, [credentials.substr(0,12), credentials.substr(12,6)])) {
    callback(null, true);
  } else {
    callback("INVALID", false);
  }
}


function runLdapServer() {
  var server = ldap.createServer();
  ldap.setupEndpoints(server, c.config.SRV_LDAP.baseDn, getUser, checkCredentials);
  ldap.startServer(server, c.config.SRV_LDAP.address, c.config.SRV_LDAP.port);
}

function runHttpServer() {
  var server = http.createServer();
  http.setupEndpoints(server, getUser, checkCredentials);
  http.startServer(server, c.config.SRV_HTTP.address, c.config.SRV_HTTP.port);
}

function runAdminServer() {
  
}


runLdapServer();
runHttpServer();
runAdminServer();
