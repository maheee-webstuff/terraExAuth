var config = require('../config.js');
var bunyan = require('bunyan');

//Config
exports.config = config;

//Logger
exports.getLogger = function (name) {
  return bunyan.createLogger({name: name, streams: config.LOG_STREAMS});
}

//Database
function getTingoDatabase() {
  var Db = require('tingodb')().Db;
  return new Db(config.DB_TINGO.path, config.DB_TINGO.options);
}

function getMongoDatabase() {
  var Db = require('mongodb').Db;
  var Server = require('mongodb').Server;
  return new Db(config.DB_MONGO.name, new Server(config.DB_MONGO.server, config.DB_MONGO.port))
}

exports.getDatabase = function () {
  if (config.DATABASE === 'TINGO') {
    return getTingoDatabase();
  } else if (config.DATABASE === 'TINGO') {
    return getMongoDatabase();
  } else {
    return null;
  }
}
