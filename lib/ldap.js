var c = require('./common.js');
var log = c.getLogger('ldap');

var ldap = require('ldapjs');


exports.createServer = function () {
  return ldap.createServer({log: c.getLogger('ldapSrv')});
};

exports.startServer = function (server, address, port) {
  server.listen(port || 389, address || '127.0.0.1', function() {
    log.info('LDAP server listening at: ' + server.url);
  });
};

exports.setupEndpoints = function (server, baseDn, getUser, checkCredentials) {
  function logSearch(req) {
    log.trace("SEARCH: " + req.dn.toString())
    log.trace("        filter: " + req.filter.toString());
  }
  
  function buildObject(id) {
    return {
      dn: 'cn='+id+',ou=Service Accounts,'+baseDn,
      attributes: {
	cn: id,
      }
    };
  }

  server.bind(baseDn, function(req, res, next) {
    log.trace("BIND: " + req.dn.toString());

    var user = req.dn.rdns[0].uid;

    log.debug("Got request for user: " + user);

    getUser(user, function (err, user) {
      checkCredentials(user, req.credentials, function (err, result) {
	if (err || !result) {
	  res.end(new ldap.InvalidCredentialsError());
	} else {
	  res.end();
	}
      });
    });

  });

  server.search(baseDn, function(req, res, next) {
    logSearch(req);
    
    var userId = req.filter.value;
    
    log.debug("Got search request for user: " + user);
    
    getUser(userId, function (err, user) {
      for (var i in user.groups) {
	res.send(buildObject(user.groups[i]), true);
      }
  
      res.end();
    });    
  });
};
